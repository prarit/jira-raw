VERSION ?= 0.0.1
GOURL ?= gitlab.com/prarit/jira-raw

build:
	GO111MODULE=on go build $(GOURL)

install: build
	GO111MODULE=on go install $(GOURL)

.PHONY: build install
