package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	jira "github.com/andygrunwald/go-jira"
	"github.com/spf13/cobra"
	// This is useful with spew.Dump(struct), ie) spew.Dump(issue)
	//"github.com/davecgh/go-spew/spew"
	"gitlab.com/prarit/jira-raw/internal/config"
)

var (
	client *jira.Client
	timeFormat string = "2006-01-02 15:04:05"
	timeFormatTZ string = "2006-01-02T15:04:05.000+0000"
	showCustomFieldIDs bool
	userSelectedFields []string
	noEscapedText bool
)

func contains(names []string, name string) bool {
	for _, a := range names {
		if a == name {
			return true
		}
	}
	return false
}

func attribute(name string, value string) {
	if len(userSelectedFields) == 0 || contains(userSelectedFields, name) {
		fmt.Printf("attribute[%s]: %s\n", name, value)
	}
}

func attribute_time(name string, jiratime jira.Time) {
	t := time.Time(jiratime)
	attribute(name, t.Format(timeFormat))
}

func attribute_user(name string, jirauser *jira.User) {
	u := jirauser.DisplayName + " <" + jirauser.Name + ">"
	attribute(name, u)
}

func attribute_int(name string, value int) {
	attribute(name, strconv.Itoa(value))
}

var rootCmd = &cobra.Command{
	Use:   "jira-dump <jira-issue-id>",
	Short: "Dump jira issue variables.",
	Long:  ``,
	Args:  cobra.RangeArgs(1, 1),
	Run: func(cmd *cobra.Command, args []string) {

		issueID := args[0]

		// this retrieves the list of fields for issues
		rawfields, _, err := client.Field.GetList()
		if err != nil {
			log.Fatal(err)
		}

		thisIssueFields := make(map[string]string)
		for _, field := range rawfields {
			thisIssueFields[field.ID] = field.Name
		}

		issue, _, err := client.Sprint.GetIssue(issueID, nil)
		if err != nil {
			log.Fatal(err)
		}

		customFields, _, err := client.Issue.GetCustomFields(issueID)
		if err != nil {
			log.Fatal(err)
		}
		for key, value := range customFields {

			var cfkey string
			if !showCustomFieldIDs {
				cfkey = thisIssueFields[key]
			} else {
				cfkey = key + "|" + thisIssueFields[key]
			}
			attribute(cfkey, value)
		}

		attribute("Project Key", issue.Fields.Project.Key)
		attribute("Project Name", issue.Fields.Project.Name)
		attribute_time("Issue Resolution Date", issue.Fields.Resolutiondate)
		attribute_time("Issue Created", issue.Fields.Created)

		if issue.Fields.Watches.IsWatching {
			watchlist := ""
			for _, w := range issue.Fields.Watches.Watchers {
				watchlist = watchlist + ", " + w.Name
			}
			attribute("watchers:", watchlist)
		} else {
			attribute("watchers:", "")
		}

		attribute_user("assignee", issue.Fields.Assignee)
		attribute_time("createdDate", issue.Fields.Updated)
		if noEscapedText {
			attribute("description", issue.Fields.Description)
		} else {
			attribute("description", strconv.Quote(issue.Fields.Description))
		}
		attribute("summary", issue.Fields.Summary)
		attribute_user("creator", issue.Fields.Creator)
		attribute_user("reporter", issue.Fields.Reporter)

		components := ""
		for _, c := range issue.Fields.Components {
			components = components + ", " + c.Name
		}
		attribute("component", components)

		attribute("status", issue.Fields.Status.Name)
		attribute("statusCategory", issue.Fields.Status.StatusCategory.Name)

		// SKIP Progress
		// SKIP AggregateProgress
		// SKIP TimeTracking

		attribute_int("timespent", issue.Fields.TimeSpent)
		attribute_int("timeestimate", issue.Fields.TimeEstimate)

		// SKIP Worklog
		// SKIP IssueLink: TODO need an example of links

		// Dump out the entire body as one large string.  This
		// section dumps each comment, author, dates, etc., and
		// combines them into one giant string
		Body := ""
		for _, c := range issue.Fields.Comments.Comments {
			author := c.Author.DisplayName + " <" + c.Author.Name + ">"
			utime, err := time.Parse(timeFormatTZ, c.Updated)
			if err != nil {
				fmt.Println(err)
				break
			}

			ctime, err := time.Parse(timeFormatTZ, c.Created)
			if err != nil {
				fmt.Println(err)
				break
			}
			commentNote := "Created by"
			if utime.After(ctime) {
				commentNote = "Updated by"
			}

			commentNote += " " + author + " at " + utime.Format(timeFormat)
			Body = Body + commentNote + " :\n" + c.Body + "\n\n"
		}
		if noEscapedText {
			attribute("comment", Body)
		} else {
			attribute("comment", strconv.Quote(Body))
		}

		fixVersions := ""
		for _, f := range issue.Fields.FixVersions {
			fixVersions = fixVersions + f.Name + ", "
		}
		attribute("fixVersion", fixVersions)

		affectsVersions := ""
		for _, a := range issue.Fields.AffectsVersions {
			affectsVersions = affectsVersions + a.Name + ", "
		}
		attribute("affectsVersion", affectsVersions)

		attribute("labels", strings.Join(issue.Fields.Labels, ","))

		// SKIP SubTasks
		// SKIP Attachments

		if issue.Fields.Epic != nil {
			attribute("epic", issue.Fields.Epic.Key)
		}
		if issue.Fields.Sprint != nil {
			attribute("sprint", issue.Fields.Sprint.Name)
		} else {
			attribute("sprint", "")
		}
		if issue.Fields.Parent != nil {
			attribute("parent", issue.Fields.Parent.Key)
		} else {
			attribute("parent", "")
		}

		attribute_int("aggregateTimeOriginalEstimate", issue.Fields.AggregateTimeOriginalEstimate)
		attribute_int("aggregateTimeSpent", issue.Fields.AggregateTimeSpent)
		attribute_int("aggregateTimeEstimate", issue.Fields.AggregateTimeEstimate)
	}, // end of func(cmd *cobra.Command, args []string)
}

func main() {

	var err error

	// Load the .env file
	jira_token, jira_url := config.LoadMainConfig()

	// Create a BasicAuth Transport object
	tp := jira.BearerAuthTransport{
		Token: jira_token,
	}
	// Create a new Jira Client
	client, err = jira.NewClient(tp.Client(), jira_url)
	if err != nil {
		log.Fatal(err)
	}

	me, _, err := client.User.GetSelf()
	if err != nil || me.DisplayName == "" {
		log.Fatal(err)
	}

	err = rootCmd.Execute()
	if err != nil && err.Error() != "" {
		fmt.Println(err)
	}
}

func init() {
	rootCmd.Flags().BoolVarP(&showCustomFieldIDs, "show-customfields", "", false, "show the customfield IDs instead of the customfield names")
	rootCmd.Flags().StringSliceVarP(&userSelectedFields, "fields", "", []string{}, "specify a comma-separated list of fields for output")
	rootCmd.Flags().BoolVarP(&noEscapedText, "noescapedtext", "", false, "show raw text (ie, no escaped characters)")
}
